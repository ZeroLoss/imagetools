import sys
import os

if __name__ == '__main__':
    # In annotation file
    # Expects to point to a folder that has since been split with training split
    annotations = sys.argv[1]

    train_annotation = f'{annotations}.train'
    test_annotation = f'{annotations}.test'

    with open(annotations) as in_ann, \
            open(train_annotation, 'w') as out_train, \
            open(test_annotation, 'w') as out_test:
        lines = in_ann.readlines()

        base_path = os.path.dirname(lines[0].split()[0])
        train_dir = f'{base_path}-train'
        test_dir = f'{base_path}-test'

        train_files = {fn for fn in os.listdir(train_dir)}
        test_files = {fn for fn in os.listdir(test_dir)}

        for line in lines:
            fn = os.path.basename(line.split()[0])

            if fn in train_files:
                out_train.write(line)
            elif fn in test_files:
                out_test.write(line)
