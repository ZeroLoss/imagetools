import sys
import random
import pathlib
import os
import shutil

RATIO = 0.8
SEED = 1234

if __name__ == '__main__':
    # In directory full of images
    in_dir = sys.argv[1]
    # Note: run this file with the current working directory
    # the directory that contains this folder

    train_dir = f'{in_dir}-train'
    test_dir = f'{in_dir}-test'
    pathlib.Path(train_dir).mkdir(parents=True, exist_ok=True)
    pathlib.Path(test_dir).mkdir(parents=True, exist_ok=True)

    files = list(os.path.abspath(os.path.join(in_dir, fn)) for fn in os.listdir(in_dir))
    random.seed(SEED)
    random.shuffle(files)

    split_point = int(RATIO * len(files))
    train = files[:split_point]
    test = files[split_point:]

    for file in train:
        shutil.copy(file, train_dir)

    for file in test:
        shutil.copy(file, test_dir)
