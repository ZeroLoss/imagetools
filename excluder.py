import os
from collections import defaultdict

REMOVE_DIR = 'buds'
IN_PREFIX = 'images/'
IN_ANNOTATION = 'all-positives.txt'
OUT_ANNOTATION = 'all-positives-no-buds.txt'


def working_format(line):
    parts = line.split()
    fn = parts[0]
    num = int(parts[1])
    pos = []
    for n in range(num):
        x, y, w, h = list(map(int, parts[2 + n * 4:6 + n * 4]))
        pos.append((x, y, w, h))
    return [fn, num, pos]


def print_format(info):
    if info[1] != 0:
        return f'{info[0]} {info[1]} {" ".join([" ".join(map(str, x)) for x in info[2]])}'
    else:
        return f'{info[0]} 0'


if __name__ == '__main__':
    with open(IN_ANNOTATION) as in_ann, open(OUT_ANNOTATION, 'w') as out_ann:
        lines = list(map(working_format, in_ann.readlines()))
        to_remove = defaultdict(list)
        for fn in os.listdir(REMOVE_DIR):
            parts = fn.split('-')
            original_fn = IN_PREFIX + parts[0]
            num = int(parts[1].split('.')[0])
            to_remove[original_fn].append(num)
        # print(to_remove)
        # print(lines)

        for info in lines:
            indices_to_remove = to_remove[info[0]]
            for index in sorted(indices_to_remove, reverse=True):
                del info[2][index]
            info[1] = len(info[2])
            out_ann.write(print_format(info) + '\n')
