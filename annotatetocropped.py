import cv2
import sys
import ntpath
import pathlib

# Config variables
SAVE_IMAGES = True  # Whether to actually save the cropped images
EXPAND_TO_SQUARE = True  # Expand into square sections
PRINT_NEW = True  # Whether to print to stdout the "new" data file (from expanding to square)
EXTENSION = '.png'  # File type to save as
RESIZE = True  # Whether to resize everything to some fixed size
DIMENSION = 64  # Width and height if resizing is true

if __name__ == '__main__':
    # In text file, generated from opencv_annotation
    textfile = sys.argv[1]
    # Out directory to put cropped images
    outdir = sys.argv[2] if len(sys.argv) >= 3 else f'{textfile}-cropped'

    if SAVE_IMAGES:
        pathlib.Path(outdir).mkdir(parents=True, exist_ok=True)

    with open(textfile) as f:
        for line in f.readlines():
            # Parse each line
            parts = line.split()
            fn = parts[0]
            img = cv2.imread(fn)
            num = int(parts[1])

            if PRINT_NEW and not RESIZE:
                print(f'{fn} {num}', end='')

            for n in range(num):
                # Grab positions
                x, y, w, h = list(map(int, parts[2 + n * 4:6 + n * 4]))

                if EXPAND_TO_SQUARE:
                    # Expand shortest edge to make 1:1 aspect ratio
                    if w < h:
                        diff = h - w
                        w = h
                        x -= diff // 2
                        # Ensure inside left/right bounds
                        if x < 0:
                            x = 0
                        elif x + w >= img.shape[1]:
                            x = img.shape[1] - w - 1
                    elif h < w:
                        diff = w - h
                        h = w
                        y -= diff // 2
                        # Ensure inside top/bottom bounds
                        if y < 0:
                            y = 0
                        elif y + h >= img.shape[0]:
                            y = img.shape[0] - h - 1

                new_fn = f'{outdir}/{ntpath.basename(fn)}-{n}{EXTENSION}'

                if SAVE_IMAGES:
                    saved_img = img[y:y + h, x:x + h]

                    if RESIZE:
                        saved_img = cv2.resize(saved_img, (DIMENSION, DIMENSION))

                    cv2.imwrite(new_fn, saved_img)

                if PRINT_NEW and not RESIZE:
                    print(f' {x} {y} {w} {h}', end='')

                if PRINT_NEW and RESIZE:
                    print(f'{new_fn} 1 0 0 {DIMENSION} {DIMENSION}')

            if PRINT_NEW and not RESIZE:
                print()
