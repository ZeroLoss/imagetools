import sys
import os
import cv2
import numpy as np
import random
import pathlib


def sharpen(img):
    kernel = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])
    return cv2.filter2D(img, -1, kernel)


def smooth(img):
    return cv2.GaussianBlur(img, (5, 5), 0, 0, cv2.BORDER_DEFAULT)


def snp(img):
    prob = 0.05
    thres = 1 - prob

    output = np.zeros(img.shape, np.uint8)
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            r = random.random()
            if r < prob:
                output[i][j] = 0  # Pepper
            elif r > thres:
                output[i][j] = 255  # Salt
            else:
                output[i][j] = img[i][j]
    return img


def noise(img):
    sigma = 0.1 ** 0.5
    h, w, c = img.shape

    gauss = np.random.normal(0, sigma, img.shape)
    gauss = gauss.reshape(h, w, c)

    return img + gauss


def contrastchange(img, c):
    return cv2.addWeighted(img, 1. + c / 127., img, 0, -c)


def flipv(img):
    res = img.copy()
    return cv2.flip(res, 1)


def savealternatives(img: np.ndarray, outfmt: str):
    operations = [
        ('sharpen', sharpen),
        ('smooth', smooth),
        ('snp', snp),
        ('noise', noise),
        ('contrastup', lambda img: contrastchange(img, 64)),
        ('contrastdown', lambda img: contrastchange(img, -64)),
        ('flipv', flipv)
    ]

    for opname, func in operations:
        cv2.imwrite(outfmt.format(opname), func(img))


if __name__ == '__main__':
    indir = sys.argv[1] if len(sys.argv) >= 2 else 'images'
    outdir = sys.argv[2] if len(sys.argv) >= 3 else 'out'

    pathlib.Path(outdir).mkdir(parents=True, exist_ok=True)

    for fn in os.listdir(indir):
        fullname = os.path.abspath(os.path.join(indir, fn))
        img = cv2.imread(fullname)
        if img is None:
            print(f'Couldn\'t read {fn}')
            continue
        savealternatives(img, f'{outdir}/{fn}-{{0}}.png')
        print(f'Done {fn}')
